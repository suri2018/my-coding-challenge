function smallestDiff(arr1, arr2) {
  let diff = null;
  let result = [];
  for (let i = 0; i < arr1.length; i++) {
    for (let j = 0; j < arr2.length; j++) {
      let curDiff = Math.abs(arr1[i] - arr2[j]);
      if (i === 0 && j === 0) {
        diff = curDiff;
      } else {
        if (curDiff < diff) {
          diff = curDiff;
          result = [arr1[i], arr2[j]];
        }
      }
    }
  }
  return result;
}

console.log(smallestDiff([-1, 5, 10, 20, 28, 3], [26, 134, 135, 15, 17]));
module.exports = smallestDiff