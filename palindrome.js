function checkPalindrome(str) {
  let newStr = str.toLowerCase();
  let letterArray = newStr.split("");
  letterArray.reverse();
  let palStr = letterArray.join("");
  if (str === palStr) {
    return true;
  }
  return false;
}
module.exports = checkPalindrome