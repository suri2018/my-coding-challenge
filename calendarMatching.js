function createSched(calendar, dailyBounds) {
  let sched = [];
  let startTime = [];
  let endTime = [];
  let startTimeInMins = [];
  let endTimeInMins = [];

  //initialize schedule arrayas
  for (let i = 0; i < 24 * 60; i++) {
    sched[i] = 0;
  }
  //convert given data to start time and end time arrays for each person
  for (let i = 0; i < calendar.length; i++) {
    startTime[i] = calendar[i][0].split(":");
    endTime[i] = calendar[i][1].split(":");
  }

  //convert the start time and end time arrays into minutes
  for (let i = 0; i < startTime.length; i++) {
    startTimeInMins[i] = Number(startTime[i][0]) * 60 + Number(startTime[i][1]);
    endTimeInMins[i] = Number(endTime[i][0]) * 60 + Number(endTime[i][1]);
  }

  //console.log(startTimeInMins)
  //block off meeting times
  for (let i = 0; i < calendar.length; i++) {
    for (let j = startTimeInMins[i] + 1; j < endTimeInMins[i] + 1; j++) {
      sched[j] = 1;
    }
  }
  //block off after hours
  let lowerTimeBound = dailyBounds[0][0].split(":");
  let totalBeforeHours =
    Number(lowerTimeBound[0]) * 60 + Number(lowerTimeBound[1]);
  for (let i = 0; i < totalBeforeHours; i++) {
    sched[i] = 1;
  }
  //block off after hours
  let upperTimeBound = dailyBounds[0][1].split(":");
  let totalAfterHours =
    Number(upperTimeBound[0]) * 60 + Number(upperTimeBound[1]);
  for (let i = totalAfterHours; i < sched.length; i++) {
    sched[i] = 1;
  }
  return sched;
}

function findMeetingTimes(
  calendar1,
  dailyBounds1,
  calendar2,
  dailyBounds2,
  meetingDuration
) {
  let sched1 = createSched(calendar1, dailyBounds1);
  let sched2 = createSched(calendar2, dailyBounds2);
  //console.log(sched1, sched2)
  //create the conflict array
  let conflict = [];
  for (let i = 0; i < sched1.length; i++) {
    conflict[i] = sched1[i] + sched2[i];
  }
  //create open times
  let counter = 0;
  let openings = [];
  for (let i = 0; i < conflict.length; i++) {
    if (conflict[i] === 0) {
      counter += 1;
      if (counter === meetingDuration) {
        openings.push(i);
        counter = 0;
      }
    } else {
      counter = 0;
    }
  }
  console.log(openings);
  let availableMeetingTimes = [];
  for (let i = 0; i < openings.length; i++) {
    let endAvailableTime = openings[i];
    let startAvailableTime = openings[i] - meetingDuration;
    let endAvailableHour = Math.floor(endAvailableTime / 60);
    let endAvailableMinutes = endAvailableTime % 60;
    let startAvailableHour = Math.floor(startAvailableTime / 60);
    let startAvailableMinutes = startAvailableTime % 60;
    let startminuteformat =
      startAvailableMinutes >= 0 && startAvailableMinutes < 10
        ? `0${startAvailableMinutes}`
        : startAvailableMinutes;
    let endminuteformat =
      endAvailableMinutes >= 0 && endAvailableMinutes < 10
        ? `0${endAvailableMinutes}`
        : endAvailableMinutes;

    let timeSlot = [
      `${startAvailableHour}:${startminuteformat}`,
      `${endAvailableHour}:${endminuteformat}`,
    ];
    availableMeetingTimes.push(timeSlot);
  }
  return availableMeetingTimes;
}

let calendar1 = [
  ["9:00", "10:30"],
  ["12:00", "13:00"],
  ["16:00", "18:00"],
];
let dailyBounds1 = [["9:00", "20:30"]];
let calendar2 = [
  ["10:00", "11:30"],
  ["12:30", "14:30"],
  ["14:30", "15:00"],
  ["16:00", "17:00"],
];
let dailyBounds2 = [["10:00", "18:30"]];
let meetingDuration = 30;

console.log(
  findMeetingTimes(
    calendar1,
    dailyBounds1,
    calendar2,
    dailyBounds2,
    meetingDuration
  )
);
module.exports = findMeetingTimes