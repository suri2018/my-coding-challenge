function sumOfFour(arr, target) {
  let distinctNums = new Set(arr);
  let newArr = Array.from(distinctNums); // creating an array of only distinct elements
  newArr.sort((a, b) => b - a);
  let map = new Map(newArr.map((x) => [x, 0])); //create a map with key as element of the array and value 0
  let quadruples = [];
  let firstArr = newArr.filter((num) => num < target); //getting only values smaller than target
  for (let i = 0; i < firstArr.length; i++) {
    let firstNum = firstArr[i];
    let secondArr = firstArr.filter(
      (num) => num < target - firstNum && num !== firstNum
    );
    for (let j = 0; j < secondArr.length; j++) {
      let secondNum = secondArr[j];
      let thirdArr = secondArr.filter(
        (num) =>
          num < target - firstNum - secondNum &&
          num !== firstNum &&
          num !== secondNum
      );
      for (let k = 0; k < thirdArr.length; k++) {
        let thirdNum = thirdArr[k];
        let fourthNum = thirdArr.find(
          (num) =>
            num === target - firstNum - secondNum - thirdNum &&
            num !== firstNum &&
            num !== secondNum &&
            num !== thirdNum
        );
        if (fourthNum) {
          if (
            map.get(firstNum) +
              map.get(secondNum) +
              map.get(thirdNum) +
              map.get(fourthNum) <
            4
          ) {
            quadruples.push([firstNum, secondNum, thirdNum, fourthNum]);
            map.set(firstNum, 1);
            map.set(secondNum, 1);
            map.set(thirdNum, 1);
            map.set(fourthNum, 1);
          }
        }
      }
    }
  }
  return quadruples;
}
console.log(sumOfFour([7, 6, 4, -1, 1, 2], 16));

module.exports = sumOfFour
