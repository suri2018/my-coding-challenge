 function bubbleSort(arr1) {
  let swap = true;
  let num = arr1.length - 1;
  let newArr = [...arr1];

  do {
    swap = false;
    for (let i = 0; i < num; i++) {
      if (newArr[i] > newArr[i + 1]) {
        var temp = newArr[i];
        newArr[i] = newArr[i + 1];
        newArr[i + 1] = temp;
        swap = true;
      }
    }
    num--;
  } while (swap);
  return newArr;
}

console.log(bubbleSort([8, 5, 2, 9, 5, 6, 3]));
module.exports = bubbleSort


