const smallestDiff = require("./smallestDiff")
const sumOfFour = require("./sumOfFour")
const  bubbleSort  = require('./bubbleSort')
const checkPalindrome = require('./palindrome')
const findMeetingTimes = require("./calendarMatching")

//run the smallestDiff function
console.log('test for smallestDiff')
console.log(smallestDiff([-1, 5, 10, 20, 28, 3], [26, 134, 135, 15, 17]));

console.log('******************')
//run the sumOfFour function
console.log('test for sumOfFour')
console.log(sumOfFour([7, 6, 4, -1, 1, 2], 16));

console.log('******************')
//run the bubbleSort function
console.log("test for bubbleSort");
console.log(bubbleSort([8, 5, 2, 9, 5, 6, 3]));

console.log("******************");
//run the palindrome function
console.log("test for checkPalindrome");
console.log(checkPalindrome("abcdcba"));

console.log("******************");
//run the findMeetingTimes function
console.log("test for findMeetingTimes");
let calendar1 = [
  ["9:00", "10:30"],
  ["12:00", "13:00"],
  ["16:00", "18:00"],
];
let dailyBounds1 = [["9:00", "20:30"]];
let calendar2 = [
  ["10:00", "11:30"],
  ["12:30", "14:30"],
  ["14:30", "15:00"],
  ["16:00", "17:00"],
];
let dailyBounds2 = [["10:00", "18:30"]];
let meetingDuration = 30;

console.log(
  findMeetingTimes(
    calendar1,
    dailyBounds1,
    calendar2,
    dailyBounds2,
    meetingDuration
  )
);

console.log("******************");

